from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

class ListCheckOutFilesWidget(QWidget):
    def __init__(self, ListAssets, icon):
        super().__init__()

        self.scroll = QScrollArea()
        self.gridbox = QGridLayout()
        self.scrollWidget = QWidget()
        self.vbox01 = QVBoxLayout()

        self.VirtualTable = []
        self.UpdateList(ListAssets)
        self.scrollWidget.setLayout(self.gridbox)

        self.scroll.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.scroll.setWidgetResizable(True)
        self.scroll.setWidget(self.scrollWidget)

        self.vbox01.addWidget(self.scroll)
        self.setLayout(self.vbox01)

        self.setWindowIcon(icon)
        self.setWindowTitle("Check-out assets list")
        self.show()
        # self.setFixedHeight(self.height())

    def UpdateList(self, ListAssets):
        oldListSize = len(self.VirtualTable)
        newListSize = len(ListAssets)
        
        updateRange = max(0, min(oldListSize, newListSize))
        for i in range(updateRange):
            print("Update asset " + str(i) + " " + str(ListAssets[i][0]))
            self.VirtualTable[i][0].setText(ListAssets[i][0])
            self.VirtualTable[i][1].setText(ListAssets[i][1])
            self.VirtualTable[i][2].setText(ListAssets[i][2])

        print("ListAssets " + str(oldListSize) + " " + str(newListSize))

        if oldListSize < newListSize:
            print("Adding rows assets")
            for i in range(oldListSize, newListSize):
                self.VirtualTable.append(self.CreateRow(ListAssets[i]))
                self.gridbox.addWidget(self.VirtualTable[i][0], i,0)
                self.gridbox.addWidget(self.VirtualTable[i][1], i,1)
                self.gridbox.addWidget(self.VirtualTable[i][2], i,2)
        elif oldListSize > newListSize:
            print("Removing row assets")
            for i in range(oldListSize-1, newListSize-1, -1):
                print("Removing asset " + str(i) + " " + self.VirtualTable[i][0].text())
                self.gridbox.removeWidget(self.VirtualTable[i][0])
                self.gridbox.removeWidget(self.VirtualTable[i][1])
                self.gridbox.removeWidget(self.VirtualTable[i][2])
                tmpRow = self.VirtualTable.pop(i)
                for elem in tmpRow:
                    elem.setParent(None)
                    del elem
                # self.gridbox.update()

    def CreateRow(self, assetRow):
        return [QLabel(assetRow[0]), QLabel(assetRow[1]), QLabel(assetRow[2])]