from MiscFunctions import *

from AssetsLocked import AssetsLocked
from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot

class AssetsManager(QObject):
    # Notify locks change ()
    OnLockAssetsChange = pyqtSignal(int, int, dict, dict)

    def __init__(self):
        QObject.__init__(self)
        self.assetsLocked = AssetsLocked()
        self.assetsBackup = AssetsLocked()
        self.assetsDouble = AssetsLocked()
        self.InitManager()

    def InitManager(self):
        self.added = 0
        self.removed = 0
        self.assetsAdded = {}
        self.assetsRemoved = {}

    def ResetManager(self):
        self.assetsLocked.ClearAssets()
        self.assetsBackup.ClearAssets()
        self.assetsDouble.ClearAssets()
        self.ClearAssetsDictionaries()
        self.InitManager()

    def ClearAssetsDictionaries(self):
        self.assetsAdded.clear()
        self.assetsRemoved.clear()

    def AddAsset(self, newRow):
        self.assetsLocked.AddAsset(newRow[0], newRow[1], newRow[2])

    def AddChange(self, RowChange):
        self.added = 0

        print("(AssetManager) AddChange: Adding? \"" + RowChange[0] + "\" \"" + RowChange[1] + "\" \"" + RowChange[2] + "\"")
        if self.assetsLocked.AddAsset(RowChange[0], RowChange[1], RowChange[2]) is True:
            print("(AssetManager) AddChange: Adding: " + RowChange[0])
            self.added = self.added +1
            RegisterAssetInDictionary(self.assetsAdded, RowChange[1], RowChange[0])
        print("(AssetManager) AddChange: Adding Duplicate: " + RowChange[0])
        self.assetsDouble.AddAsset(RowChange[0], RowChange[1], RowChange[2])
    
    def IsAssetLocked(self, AssetName):
        return (self.assetsLocked.FindAssetByName(AssetName) is not None)

    def LookForChanges(self):
        self.removed = 0
        idToRemove = []

        for assetRow in self.assetsLocked.GetAssetsRows():
            print("(AssetManager) LookForChanges: Assets to remove? " + str(assetRow))
            if self.assetsDouble.FindAsset(assetRow[2]) is None:
                self.removed = self.removed +1
                idToRemove.append(assetRow[2])
                RegisterAssetInDictionary(self.assetsRemoved, assetRow[1], assetRow[0])
        
        for id in idToRemove:
            self.assetsLocked.RemoveAsset(id)

        self.assetsBackup.ClearAssets()
        self.assetsDouble.ClearAssets()

        # Notify changes (how many assets added, how many removed)
        print("(AssetManager) Changes: " + str(self.added) + " " + str(self.removed)  + " " + str(self.assetsAdded) + " " + str(self.assetsRemoved))
        if self.added > 0 or self.removed > 0:
            self.OnLockAssetsChange.emit(self.added, self.removed, self.assetsAdded, self.assetsRemoved)