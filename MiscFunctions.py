import re

def DisplayStartNotification(systemtray_ref, WorkspaceTitle, Number, systemtray_icon):
    if Number > 1:
        AssetMsg = " assets are in use"
    else:
        AssetMsg = " asset is in use"
    msg = str(Number) + AssetMsg
    systemtray_ref.showMessage(WorkspaceTitle, msg, systemtray_icon)

def DisplayGenericNotification(systemtray_ref, WorkspaceTitle, Message, systemtray_icon):
    systemtray_ref.showMessage(WorkspaceTitle, Message, systemtray_icon)

def AssetDictionaryToMessage(Dictionay, action):
    msg = ""
    for User in Dictionay:
        msg = msg + User + " " + action + ":\n"
        for asset in Dictionay[User]:
            msg = msg + "  * " + str(asset) + "\n"
    return msg

def RegisterAssetInDictionary(Dictionary, User, Asset):
    print("RegisterAssetInDictionary: \"" + User + "\" \"" + Asset + "\"")
    if User in Dictionary:
        Dictionary[User].append(Asset)
        print("RegisterAssetInDictionary: Append: " + str(Dictionary))
    else:
        Dictionary.update( {User : [Asset]} )
        print("RegisterAssetInDictionary: New User Key: " + str(Dictionary))


def GetCleanString(tmpString):
    tmpStr = re.sub('[*\'b]', '', str(tmpString)).strip()
    return tmpStr[0 : len(tmpStr) -2 : ]