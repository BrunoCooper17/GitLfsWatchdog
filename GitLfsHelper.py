import os
import subprocess

from MiscFunctions import *
from AssetsLocked import AssetsLocked
from ChangesThread import ChangesThread
from SettingsWidget import SettingsWidget
from ListCheckOutFilesWidget import ListCheckOutFilesWidget
from ReleaseWidget import ReleaseWidget

from PyQt5 import QtWidgets
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QObject, QThread, QSemaphore, pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import QSystemTrayIcon, QMenu, QAction, QApplication, QFileDialog


#====================================================================================================

def QuitApp():
    QApplication.quit()

#====================================================================================================


class MyAppBase():
    workspaceFilename = ".workspace"
    settingsFilename = ".settings"
    assetsFolder = "Content"

    def __init__(self):
        self.gitUser = "dummy"
        self.workspaceDir = "."
        self.workspaceName = "(none)"
        self.timeThread = 10
        self.defaultWorkspace = "."
        self.checkOutFilesWidget = None

    def InitApp(self):
        # Create the icon
        self.icon = QIcon("icons/GitLfs_Icon.png")
        self.CreateSystemTray()
        self.CreateActionMenu()
        self.CreateMenu()

        # Get git user
        process = subprocess.Popen(["git", "config", "--global", "user.name"], stdout=subprocess.PIPE, bufsize=1)
        for line in iter(process.stdout.readline, b''):
            self.gitUser = GetCleanString(line)
            print("User? " + self.gitUser)

        self.LoadSettingsFile()
        self.changesThreads = ChangesThread(self.gitUser, self.tray, self.icon)
        self.changesThreads.SetTimeLookThread(self.timeThread)
        self.LoadWorkspace()

        # Add the menu to the tray
        self.tray.setContextMenu(self.menu)
        self.tray.messageClicked.connect(self.OpenListCheckOutFiles)
        # DisplayStartNotification(self.tray, "BusterBoosterBus", 5, self.icon)
        # DisplayLockNotification(self.tray, "BusterBoosterBus", "BrunoCooper17", "BP_Bus_Base", self.icon)
        self.changesThreads.start()


    def CreateSystemTray(self):
        # Create systemtray
        self.tray = QSystemTrayIcon()
        self.tray.setIcon(self.icon)
        self.tray.setVisible(True)


    def CreateActionMenu(self):
        # Actions for the menu
        self.actionWorkspace = QAction("(none)")
        self.actionListAsset = QAction("List check-out assets")
        self.actionLockAsset = QAction("Check-out assets")
        self.actionUnlockAssets = QAction("Release assets")
        self.actionSettings = QAction("Settings")
        self.actionQuit = QAction("Quit")

        self.actionWorkspace.triggered.connect(self.ShowOpenDirectory)
        self.actionListAsset.triggered.connect(self.OpenListCheckOutFiles)
        self.actionLockAsset.triggered.connect(self.ShowOpenAsset)
        self.actionUnlockAssets.triggered.connect(self.OpenReleaseWidget)
        self.actionSettings.triggered.connect(self.OpenSettingsWidget)
        self.actionQuit.triggered.connect(QuitApp)


    def CreateMenu(self):
        # Create the menu
        self.menu = QMenu()
        self.menu.addSection("Workspace")
        self.menu.addAction(self.actionWorkspace)
        self.menu.addSection("Git Lfs")
        self.menu.addAction(self.actionListAsset)
        self.menu.addAction(self.actionLockAsset)
        self.menu.addAction(self.actionUnlockAssets)
        self.menu.addSection("Config")
        self.menu.addAction(self.actionSettings)
        self.menu.addSeparator()
        self.menu.addAction(self.actionQuit)


    def SetEnableGitLfs(self, Workspace, bEnable):
        self.actionWorkspace.setText(Workspace)
        self.actionListAsset.setEnabled(bEnable)
        self.actionLockAsset.setEnabled(bEnable)
        self.actionUnlockAssets.setEnabled(bEnable)


    def ShowOpenDirectory(self):
        directory = QFileDialog.getExistingDirectory(None, "Select Workspace", self.defaultWorkspace, QFileDialog.ShowDirsOnly)
        if os.path.isdir(directory + "/.git") is True:
            print("Maybe it's a valid directory :)")
            
            valid, self.workspaceName, self.assetsFolder = self.IsUnityUnreal(directory)
            if valid is True:
                self.workspaceDir = directory
                self.WriteWorkspace()

                self.SetEnableGitLfs(self.workspaceName, True)
                self.changesThreads.ChangeWorkSpace(self.workspaceDir, self.workspaceName)
        # else:
        #     print("No Git Directory")

    def IsUnityUnreal(self, path):
        # Check if it's an unreal project (just look for *.uproject file)
        for files in os.listdir(path):
            if files.endswith(".uproject"):
                return True, files.replace(".uproject", ""), "Content"
        
        # Check if it's unity
        for files in os.listdir(path):
            if(files == "Assets"):
                dirSplit = path.split("/")
                return True, dirSplit[len(dir) -1], "Assets"

        return False, "(none)", ""


    def ShowOpenAsset(self):
        assets = QFileDialog.getOpenFileNames(None, "Select asset", self.workspaceDir + "/" + self.assetsFolder, "Asset (*.*)")
        if len(assets[0]) > 0:
            self.changesThreads.LockAssets(assets[0])

    def OpenSettingsWidget(self):
        print("Show Settings Widget")
        self.settingsWidget = SettingsWidget(self.defaultWorkspace, self.timeThread, self.icon)
        self.settingsWidget.OnSaveSettings.connect(self.UpdateSettings)

    def UpdateSettings(self, newWorkspaceBase, newTime):
        self.defaultWorkspace = newWorkspaceBase
        self.timeThread = newTime
        self.changesThreads.SetTimeLookThread(newTime)
        self.WriteSettingsFile()

    def OpenListCheckOutFiles(self):
        if (self.checkOutFilesWidget is None) or (self.checkOutFilesWidget.isHidden()):
            self.checkOutFilesWidget = ListCheckOutFilesWidget(self.changesThreads.GetListAssetsLocked(), self.icon)
            self.changesThreads.OnLockAssetsChange.connect(self.UpdateCheckOutList)

    def UpdateCheckOutList(self, ListAssets):
        if self.checkOutFilesWidget is not None:
            if self.checkOutFilesWidget.isHidden():
                self.changesThreads.OnLockAssetsChange.disconnect(self.UpdateCheckOutList)
                self.checkOutFilesWidget.destroy()
                self.checkOutFilesWidget = None
            else:
                self.checkOutFilesWidget.UpdateList(ListAssets)

    def OpenReleaseWidget(self):
        self.releaseWidget = ReleaseWidget(self.changesThreads.GetListAssetsLocked(), self.gitUser, self.tray, self.workspaceDir, self.workspaceName, self.icon)

    def LoadWorkspace(self):
        if os.path.isfile(self.workspaceFilename) is True:
            # print("File exist!")
            configFile = open(self.workspaceFilename)
            self.workspaceDir = configFile.readline().replace('\n','')
            self.workspaceName = configFile.readline().replace('\n','')
            self.assetsFolder = configFile.readline().replace('\n','')

            self.SetEnableGitLfs(self.workspaceName, True)
            self.changesThreads.ChangeWorkSpace(self.workspaceDir, self.workspaceName)
            
            # print("Workspace Dir: " + self.workspaceDir)
            # print("Workspace Name: \"" + self.workspaceName + "\"\n")

        else:
            # print("File doesn't exist :(\n")
            self.SetEnableGitLfs("(none)", False)

    def WriteWorkspace(self):
        configWorkspace = open(self.workspaceFilename, "w")
        configWorkspace.write(self.workspaceDir + "\n")
        configWorkspace.write(self.workspaceName + "\n")
        configWorkspace.write(self.assetsFolder + "\n")
        configWorkspace.close()

    def LoadSettingsFile(self):
        if os.path.isfile(self.settingsFilename) is True:
            settings = open(self.settingsFilename)
            self.timeThread = int(settings.readline().replace('\n',''))
            self.defaultWorkspace = settings.readline().replace('\n','')
        else:
            self.timeThread = 10
            self.defaultWorkspace = "."
            self.WriteSettingsFile()
        
    def WriteSettingsFile(self):
        settings = open(self.settingsFilename, "w")
        settings.write(str(self.timeThread) + "\n")
        settings.write(self.defaultWorkspace + "\n")
        settings.close()


#====================================================================================================


# START APPLICATION
app = QApplication([])
app.setQuitOnLastWindowClosed(False)
myApp = MyAppBase()
myApp.InitApp()
app.exec_()
