import traceback

class AssetsLocked():
    def __init__(self):
        self.AssetsList = []

    def AddAsset(self, assetDir, user, idAsset):
        # traceback.print_stack()
        # print("(AssetsLocked) AddAsset: Add???: " + str(idAsset) + " " + str(assetDir) + " " + str(user) )
        if self.FindAsset(idAsset) is None:
            self.AssetsList.append([assetDir, user, idAsset])
            # print("(AssetsLocked) AddAsset: Added: " + str(self.AssetsList[len(self.AssetsList) -1]) )
            return True
        return False
    
    def RemoveAsset(self, idAsset):
        # print("(AssetsLocked) RemoveAsset: ******************** STACK TRACE ********************")
        # traceback.print_stack()
        # print("(AssetsLocked) RemoveAsset: ******************** STACK TRACE ********************\n")
        assetRow = self.FindAsset(idAsset)
        if assetRow is not None:
            self.AssetsList.remove(assetRow)

    def FindAsset(self, idAsset):
        for assetRow in self.AssetsList:
            # print("(AssetsLocked) FindAsset: \"" + str(assetRow[2]) + "\"" + " == " + "\"" + str(idAsset) + "\"")
            if(assetRow[2] == idAsset):
                # print("(AssetsLocked) FindAsset: FOUND \"" + str(assetRow) + "\"")
                return assetRow
        return None

    def FindAssetByName(self, AssetName):
        for assetRow in self.AssetsList:
            # print("(AssetsLocked) FindAssetByName \"" + str(assetRow[1]) + "\"" + " == " + "\"" + str(AssetName) + "\"")
            if(assetRow[0] == AssetName):
                # print("(AssetsLocked) FindAssetByName: FOUND \"" + str(assetRow) + "\"")
                return assetRow
        return None

    def GetAssetsRows(self):
        return self.AssetsList

    def ClearAssets(self):
        # print("(AssetsLocked) ClearAssets: ******************** STACK TRACE ********************")
        # traceback.print_stack()
        # print("(AssetsLocked) ClearAssets: ******************** STACK TRACE ********************\n")
        self.AssetsList.clear()