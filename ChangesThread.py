import subprocess

from MiscFunctions import *
from AssetsManager import AssetsManager
from PyQt5.QtCore import QObject, QThread, QSemaphore, pyqtSignal, pyqtSlot

class ChangesThread(QThread):
    OnLockAssetsChange = pyqtSignal(list)

    def __init__(self, currentGitUser, tray, newIcon):
        QThread.__init__(self)
        self.lookSemaphore = QSemaphore(1)
        self.systemTray = tray
        self.trayIcon = newIcon
        self.currentGitUser = currentGitUser

        self.bPaused = True
        self.assetsManager = AssetsManager()
        self.currentGitUser = ""
        self.currentWorkspace = ""
        self.currentWorkspaceName = ""

        self.assetsManager.OnLockAssetsChange.connect(self.NotifyThreadChange)
        print("Checking locks thread")


    def SetTimeLookThread(self, timeThread):
        self.timeThread = timeThread

    def run(self):
        while(True):
            if self.currentWorkspace is not "":
                print("THREAD: Start looking for changes! :) ====================================================================")
                self.LookForChanges()
                print("THREAD: End looking for changes!   :) ====================================================================\n")
                self.sleep(self.timeThread)
            else:
                self.sleep(1)
    
    def PauseThread(self, paused):
        self.bPaused = paused

    def GetListAssetsLocked(self):
        self.lookSemaphore.acquire()
        listAssetsCopy = self.assetsManager.assetsLocked.GetAssetsRows()
        self.lookSemaphore.release()
        return listAssetsCopy

    def NotifyThreadChange(self, Added, Removed, DictAdded, DictRemoved):
        print("(ChangesThread) NotifyThreadChange " + str(Added) + " " + str(Removed) + " " + str(DictAdded) + " " + str(DictRemoved))
        msg = AssetDictionaryToMessage(DictAdded, "check-out")
        msg = msg + AssetDictionaryToMessage(DictRemoved, "release")
        msg = msg[0 : len(msg) -1 : ]
        DisplayGenericNotification(self.systemTray, self.currentWorkspaceName, msg, self.trayIcon)
        self.OnLockAssetsChange.emit(self.GetListAssetsLocked())

    def LockAssets(self, ListAssets):
        self.lookSemaphore.acquire()
        count = 0
        tmpDictionary = { self.currentGitUser:[] }
        for asset in ListAssets:
            if asset.startswith(self.currentWorkspace) is True:
                asset = asset.replace(self.currentWorkspace + "/", "")
                # print("Asset to lock? \"" + asset + "\"")
                if self.assetsManager.IsAssetLocked(asset) is False:
                    process = subprocess.Popen(["git", "lfs", "lock", asset],
                                                cwd=self.currentWorkspace,
                                                stdout=subprocess.PIPE,
                                                bufsize=1)
                    #for line in iter(process.stdout.readline, b''):
                        #print("Return line: " + str(line))
                    count = count +1
                    tmpDictionary[self.currentGitUser].append(asset)
        if count > 0:
            msg = self.currentGitUser
            msg = msg + " " + AssetDictionaryToMessage(tmpDictionary, "locked")
            msg = msg[0 : len(msg) -1 : ]
        else:
            msg = "Nothing was locked :(\nCheck if the assets are already locked"
        DisplayGenericNotification(self.systemTray, self.currentWorkspaceName, msg, self.trayIcon)
        self.lookSemaphore.release()

    def ChangeWorkSpace(self, newWorkspace, newWorkspaceName):
        self.lookSemaphore.acquire()

        self.currentWorkspace = newWorkspace
        self.currentWorkspaceName = newWorkspaceName
        self.assetsManager.ResetManager()
        assetsLock = self.CheckLfsLocksCommand()
        
        for gitLock in assetsLock:
            self.assetsManager.AddAsset(gitLock)
        DisplayStartNotification(self.systemTray, newWorkspaceName, len(assetsLock), self.trayIcon)
        
        self.lookSemaphore.release()


    def LookForChanges(self):
        self.lookSemaphore.acquire()

        self.assetsManager.ClearAssetsDictionaries()
        
        print("(ChangesThread) Look for changes in: " + self.currentWorkspace)
        assetsLock = self.CheckLfsLocksCommand()
        # print("(ChangesThread) LookForChanges: " + str(assetsLock))
        for gitLock in assetsLock:
            print("(ChangesThread) LookForChanges: LOOP " + str(gitLock))
            self.assetsManager.AddChange(gitLock)
        
        print("(ChangesThread) LookForChanges: Check in AssetManager... ")
        self.assetsManager.LookForChanges()
        self.lookSemaphore.release()


    def CheckLfsLocksCommand(self):
        process = subprocess.Popen(["git", "lfs", "locks"],
                                    cwd=self.currentWorkspace,
                                    stdout=subprocess.PIPE,
                                    bufsize=1)
        gitLock = []
        for line in iter(process.stdout.readline, b''):
            assetRow = GetCleanString(line).split("\\t")
            tmpRow = []
            for line in assetRow:
                tmpRow.append(line.strip())
            gitLock.append(tmpRow)
        return gitLock