from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

# Only needed for access to command line arguments
import sys

class SettingsWidget(QWidget):
    OnSaveSettings = pyqtSignal(str, int)

    def __init__(self, currentWorspace, currentTime, icon):
        super().__init__()

        self.currentWorspace = currentWorspace
        self.currentTime = int(currentTime)

        layout = QVBoxLayout()
        layoutHDirectory = QHBoxLayout()
        layoutHSaveSection = QHBoxLayout()
        self.labelTimeChanged = QLabel()
        labelDirectory = QLabel()
        self.sliderTime = QSlider()
        self.defaultDirWorkspaces = QLineEdit()
        openButton = QPushButton("Open")
        saveButton = QPushButton("Save")

        self.labelTimeChanged.setText("Check for changes? (Every " + str(currentTime) + " seconds)")
        labelDirectory.setText("Default directory for workspaces")
        
        self.sliderTime.setValue(int(currentTime))
        self.sliderTime.setRange(10,30)
        self.sliderTime.setOrientation(Qt.Horizontal)
        self.sliderTime.setTickInterval(5)
        self.sliderTime.setTickPosition(QSlider.TicksAbove)
        self.sliderTime.valueChanged.connect(self.SliderTimeChanged)

        self.defaultDirWorkspaces.setReadOnly(True)
        self.defaultDirWorkspaces.setText(currentWorspace)

        openButton.setSizePolicy( QSizePolicy.Preferred, QSizePolicy.Preferred )
        saveButton.setSizePolicy( QSizePolicy.Preferred, QSizePolicy.Preferred )

        openButton.clicked.connect(self.OpenDir)
        saveButton.clicked.connect(self.SaveSetting)

        layoutHDirectory.addWidget(self.defaultDirWorkspaces)
        layoutHDirectory.addWidget(openButton)

        layoutHSaveSection.addStretch(10)
        layoutHSaveSection.addWidget(saveButton, 1)
        layoutHSaveSection.addStretch(10)

        layout.addWidget(self.labelTimeChanged)
        layout.addWidget(self.sliderTime)
        layout.addWidget(labelDirectory)
        layout.addLayout(layoutHDirectory)
        layout.addLayout(layoutHSaveSection)

        self.setLayout(layout)
        self.setWindowIcon(icon)
        self.setWindowTitle("GitLfsWatchdog Settings")
        self.show()
        # self.setFixedSize(self.width(), self.height())
        self.setFixedHeight(self.height())

    def SliderTimeChanged(self):
        self.currentTime = self.sliderTime.value()
        self.labelTimeChanged.setText("Check for changes? (Every " + str(self.currentTime) + " seconds)")

    def OpenDir(self):
        self.currentWorspace = QFileDialog.getExistingDirectory(None, "Select Workspace", self.currentWorspace, QFileDialog.ShowDirsOnly)
        self.defaultDirWorkspaces.setText(self.currentWorspace)

    def SaveSetting(self):
        self.OnSaveSettings.emit(self.currentWorspace, self.currentTime)
        self.close()