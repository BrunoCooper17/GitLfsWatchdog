import re
import subprocess

from MiscFunctions import *

from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

class ReleaseWidget(QWidget):
    def __init__(self, ListAssets, GitUser, systemTray, workspace, workspaceName, icon):
        super().__init__()

        self.scroll = QScrollArea()
        self.gridbox = QGridLayout()
        self.scrollWidget = QWidget()
        self.vbox01 = QVBoxLayout()
        layoutHSaveSection = QHBoxLayout()
        saveButton = QPushButton("Release Assets")
    
        self.ListAssets = self.CureAssetsList(ListAssets, GitUser)
        self.systemTray = systemTray
        self.workspace = workspace
        self.workspaceName = workspaceName
        self.icon = icon

        self.VirtualTable = []
        self.CreateList(self.ListAssets)
        self.scrollWidget.setLayout(self.gridbox)

        self.scroll.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.scroll.setWidgetResizable(True)
        self.scroll.setWidget(self.scrollWidget)

        saveButton.clicked.connect(self.ReleaseAndUpdate)

        layoutHSaveSection.addStretch(10)
        layoutHSaveSection.addWidget(saveButton)

        self.vbox01.addWidget(self.scroll)
        self.vbox01.addLayout(layoutHSaveSection)
        self.setLayout(self.vbox01)

        self.setWindowIcon(icon)
        self.setWindowTitle("Release assets")
        self.show()
        self.setFixedSize(self.width(), self.height())
        # self.setFixedHeight(self.height())


    def CureAssetsList(self, ListAssets, GitUser):
        tmpList = []
        for asset in ListAssets:
            print("(ReleaseWidget) CurateList: " + str(asset))
            if asset[1] == GitUser:
                tmpList.append(asset)
        return tmpList

    def CreateList(self, ListAssets):
        listSize = len(ListAssets)
        for i in range(0, listSize):
            self.VirtualTable.append(self.CreateRow(ListAssets[i]))
            self.gridbox.addWidget(self.VirtualTable[i][0], i,0)
            self.gridbox.addWidget(self.VirtualTable[i][1], i,1)
            self.gridbox.addWidget(self.VirtualTable[i][2], i,2)
            self.gridbox.addWidget(self.VirtualTable[i][3], i,3)

    def ReleaseAndUpdate(self):
        tmpAssetsToUnlock = []
        for row in self.VirtualTable:
            self.gridbox.removeWidget(row[0])
            self.gridbox.removeWidget(row[1])
            self.gridbox.removeWidget(row[2])
            self.gridbox.removeWidget(row[3])

            if row[0].isChecked() == True:
                tmpAssetsToUnlock.append(row[1].text())
                for elem in row:
                    elem.setParent(None)
                    del elem
                self.VirtualTable.remove(row)
        
        index = 0
        for row in self.VirtualTable:
            self.gridbox.addWidget(self.VirtualTable[index][0], index,0)
            self.gridbox.addWidget(self.VirtualTable[index][1], index,1)
            self.gridbox.addWidget(self.VirtualTable[index][2], index,2)
            self.gridbox.addWidget(self.VirtualTable[index][3], index,3)
            index = index +1
        
        for asset in tmpAssetsToUnlock:
            print("(ReleaseWidget) Unlocking " + asset)
            self.UnlockAsset(asset)


    def CreateRow(self, assetRow):
        return [QCheckBox(), QLabel(assetRow[0]), QLabel(assetRow[1]), QLabel(assetRow[2])]

    def UnlockAsset(self, asset):
        success = True
        process = subprocess.Popen(["git", "lfs", "unlock", asset],
                                    cwd=self.workspace,
                                    stderr=subprocess.PIPE,
                                    bufsize=1)
        for line in iter(process.stderr.readline, b''):
            success = False
        return success
